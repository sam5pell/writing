---
title: "let me explain"
date: 2020-01-20T19:29:41-05:00
draft: false
showDate: true
---

If you've somehow made it here hello - please leave. If you're *still* here let
me make a few things clear to you. This is most definitely not meant for you, so
if you decide to stay and explore don't tell me. Ever. Please. Sometimes a girl
wants a publicly hosted website all to herself. Is that so hard to understand?

To be complete clear, I do actually have a reason for this and, if you would like,
you have my permission to explore as much as you would like and, if you feel so
inclined, you're more than welcome to speak to me about any of the nonsense you
find here. But before you do so here a few disclaimers to eliminate a bit of my
anxiety about sharing really anything I do with anyone else:
1. My goal here is to simply have a space to write whatever I want, however I want.
I am doing this for my own pleasure and not necessarily to inform, inspire, or
entertain anyone else. There are just a lot of words stuck in my head that Sometimes
I just need to get out.
2. The reason I chose to make this public is that it adds just enough exposure that
I know I will hold myself to a higher standard than if I were to keep these private.
If there is any chance that someone might see them I will try harder. It's that simple.
3. I suck at spelling.
4. Despite what I already said, if you have some constructive feedback I would
actually like to hear it. Gotta get over my fear of criticism eventually, so why
not start working on that now?

If you've come this far thank you. I appreciate you. Hope you find something that
you enjoy if you choose to explore.

:)
